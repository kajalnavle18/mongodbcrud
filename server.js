var express = require("express");
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var dbdetails =require('./db.json');
var app = express();

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ type: 'application/json' }));

var http = require('http').createServer(app);


app.post("/insert",function(req,response){
    MongoClient.connect(dbdetails.dburl,{useUnifiedTopology: true}, function(err, db) {
        if (err) throw err;
        var dbo = db.db(dbdetails.db);
       
        dbo.collection(dbdetails.collection).insertOne(req.body, function(err, res) {
            console.log("res,",res,err);
          if (err){
             response.send({"msg":"Something Went Wrong",payload:err})
          }else{
            console.log("1 document inserted");
            db.close();
           response.send({"msg":"Data Insert SuccessFully",payload:res});
          }
        
        });
      });

})

app.get("/find/:name",function(req,response){
    MongoClient.connect(dbdetails.dburl,{useUnifiedTopology: true}, function(err, db) {
        if (err) throw err;
        var dbo = db.db(dbdetails.db);
        let query = {name:`${req.params.name}`};
        dbo.collection(dbdetails.collection).find(query).toArray(function(err, res) {
            if (err){
               
             response.send({"msg":"Something Went Wrong",payload:err})
            }else{
              console.log("document found",res,err);
              db.close();
             response.send({"msg":"Data Find Successfully",payload:res});
            }
        });
      });

})

app.delete("/delete/:name",function(req,response){
    MongoClient.connect(dbdetails.dburl,{useUnifiedTopology: true}, function(err, db) {
        if (err) throw err;
        var dbo = db.db(dbdetails.db);
        let query = {name: `${req.params.name}` };
        console.log("query--->",query);
        dbo.collection(dbdetails.collection).deleteOne(query, function(err, obj) {
            if (err) 
            {
                response.send({"Success":false,"msg":"Data not Deleted","payload":err});
                db.close();
            }else{
              response.send({"Success":true,"msg":"Data  Deleted","payload":obj});
              db.close();
            }
          });
      });

})

app.post("/update",function(req,response){
    MongoClient.connect(dbdetails.dburl, function(err, db) {
        if (err) throw err;
        var dbo = db.db(dbdetails.db);
        var myquery = { name : `${req.body.name}` };
        var newvalues = { $set: {gender: `${req.body.gender}`, address: `${req.body.address}` } };
        dbo.collection(dbdetails.collection).updateOne(myquery, newvalues, function(err, res) {

          if (err) 
          {
              response.send({"Success":false,"msg":"Data not Updated","payload":err});
              db.close();
          }else{
            console.log("1 document updated");
            response.send({"Success":true,"msg":"Data  Updated","payload":res});
            db.close();
          }
          
        
         
        });
      });
})



app.use(express.static('public'));
var port = process.env.PORT || 8081;


http.listen(port, function () {
    console.log('Express server running on *:' + port);
    
});



